﻿using System;
using System.Collections.Generic;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             * Aplicando los principios de SOLID el codigo es mucho mas facil de leer, de implementar,
             * Sin importar que se tenga mas clases, la facilidad de uso es mucho mas ventajosa
             */
            Console.WriteLine("Biblioteca, refactorizado");

            Libro fic1 = new Ficcion("Ficcion1", "author1", 100);
            Libro fic2 = new Ficcion("Ficcion2", "author2", 200);
            Libro fic3 = new Ficcion("Ficcion3", "author3", 300);
            Libro fic4 = new Ficcion("Ficcion4", "author4", 400);
            Libro inf1 = new Infantil("Infantil1", "author5", 110);
            Libro inf2 = new Infantil("Infantil2", "author6", 210);
            Libro inf3 = new Infantil("Infantil3", "author7", 310);
            Libro inf4 = new Infantil("Infantil4", "author8", 410);

            List<Libro> libros = new List<Libro>();
            libros.Add(fic1);
            libros.Add(fic2);
            libros.Add(fic3);
            libros.Add(fic4);
            libros.Add(inf1);
            libros.Add(inf2);
            libros.Add(inf3);
            libros.Add(inf4);

            Biblioteca biblio = new Biblioteca(libros);

            Console.WriteLine("Lista de libros");
            foreach (Libro libro in libros)
            {
                Console.WriteLine("Libro: {0} - Autor: {1} - Precio: {2}", libro.nombre, libro.autor, libro.precio);
            }

            Persona p1 = new Persona("Roberto", 77887788, 6622123);
            p1.PedirLibro(biblio, fic2);
            p1.PedirLibro(biblio, fic4);
            p1.PedirLibro(biblio, inf1);
            p1.PedirLibro(biblio, inf3);

            Console.WriteLine("\nLista de libros actualizada");
            foreach (Libro libro in libros)
            {
                Console.WriteLine("Libro: {0} - Autor: {1} - Precio: {2}", libro.nombre, libro.autor, libro.precio);
            }

            Console.ReadKey();
        }
    }
}
