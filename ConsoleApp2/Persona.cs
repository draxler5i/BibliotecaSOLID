﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    public class Persona
    {
        public string nombre { get; }
        public int numero { get; }
        public int ci { get; }
        private List<Libro> libros;
        
        public Persona(string nombre, int numero, int ci)
        {
            this.nombre = nombre;
            this.numero = numero;
            this.ci = ci;
        }

        public void PedirLibro(Biblioteca biblio, Libro libro)
        {
            biblio.PrestarLibro(libro);
            if (libros == null)
            {
                libros = new List<Libro>();
            }
            libros.Add(libro);
        }

        public void DevolverLibro(Biblioteca biblio, Libro libro)
        {
            if (libros.Remove(libro))
            {
                biblio.AddLibro(libro);
            }
        }
    }
}
