﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    public class Biblioteca
    {
        private List<Libro> libros;

        public Biblioteca(List<Libro> libros)
        {
            this.libros = libros;
        }

        public void AddLibro(Libro libro)
        {
            libros.Add(libro);
        }
        
        public void PrestarLibro(Libro libro)
        {
            libros.Remove(libro);
        }
    }
}
