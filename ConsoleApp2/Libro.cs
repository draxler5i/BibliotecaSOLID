﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    public abstract class Libro
    {
        public string nombre { get; }
        public string autor { get; }
        public int precio { get; }
        
        public Libro(string nombre, string autor, int precio)
        {
            this.nombre = nombre;
            this.autor = autor;
            this.precio = precio;
        }
    }
}
